AntiFeatures:UpstreamNonFree
Categories:Reading
License:MIT
Web Site:http://manuelmaly.com/blog/HN-Hacker-News-Reader
Source Code:https://github.com/manmal/hn-android
Issue Tracker:https://github.com/manmal/hn-android/issues

Auto Name:HN
Summary:Read and comment on tech news
Description:
Hacker News client with a focus on reliability and usability.

* View articles in ViewText, Google, or your system browser (you can set each as default)
* Upvote stories (long-press on story)
* Collapse and expand comments by tapping on them
* Choose from 3 text sizes
* Switch between stories and comments with one tap
* View "more" stories than only the HN frontpage

A proprietary analytics library was removed before building.
.

Repo Type:git
Repo:https://github.com/manmal/hn-android.git

Build:1.9,10
    commit=313f0c48
    rm=libs/libGoogleAnalytics.jar
    srclibs=NoAnalytics@158a4a
    prebuild=echo 'android.library.reference.1=$$NoAnalytics$$' >> project.properties && \
        mv androidannotations-2.5.jar libs/ && \
        rm libs/androidannotations-2.5-api.jar

Build:1.9.1,11
    commit=RELEASE-1.9.1
    rm=libs/libGoogleAnalytics.jar
    srclibs=NoAnalytics@158a4a
    prebuild=echo 'android.library.reference.1=$$NoAnalytics$$' >> project.properties && \
        mv androidannotations-2.5.jar libs/ && \
        rm libs/androidannotations-2.5-api.jar

Build:1.9.5,15
    commit=ac1ea46fee0f9e0
    rm=libs/libGoogleAnalytics.jar
    srclibs=1:NoAnalytics@d38ae535b543b
    prebuild=mv androidannotations-2.5.jar libs/ && \
        rm libs/androidannotations-2.5-api.jar

Build:1.9.6,16
    commit=build-16
    rm=libs/libGoogleAnalytics.jar
    srclibs=1:NoAnalytics@d38ae535b543b
    prebuild=mv androidannotations-2.5.jar libs/ && \
        rm libs/androidannotations-2.5-api.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.9.6
Current Version Code:16

