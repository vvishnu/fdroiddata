Categories:Games
License:GPLv3
Web Site:https://code.google.com/p/droidzebra
Source Code:https://code.google.com/p/droidzebra/source
Issue Tracker:https://code.google.com/p/droidzebra/issues
Donate:http://droidzebra.appspot.com/droidzebra/donate

Summary:Reversi game
Description:
DroidZebra is advanced Reversi program based on Zebra Othello Engine written by Gunnar Andersson. Features:
* difficulty levels from beginner (practice mode) to world-class play
* opening book with more than 500,000 positions
* unlimited undo
.

Repo Type:git
Repo:https://code.google.com/p/droidzebra

Build:1.4,10
    commit=1e08eec
    subdir=project
    buildjni=yes

Build:1.4.1,12
    commit=d2e47f982c30
    subdir=project
    buildjni=yes

Build:1.5,13
    disable=Source unknown - unclear commit history and no tags

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5
Current Version Code:13

