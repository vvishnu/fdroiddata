Categories:Internet
License:GPLv3
Web Site:http://shubhamchaudhary.in
Source Code:https://github.com/shubhamchaudhary/logmein-android
Issue Tracker:https://github.com/shubhamchaudhary/logmein-android/issues

Auto Name:logmein
Summary:Login to campus networks
Description:
Automatically login to university campus networks.
.

Repo Type:git
Repo:https://github.com/shubhamchaudhary/logmein-android.git

Build:0.3,3
    disable=missing appcompat, vercode
    commit=v0.3

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.4.1
Current Version Code:7

