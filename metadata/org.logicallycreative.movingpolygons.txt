Categories:Wallpaper
License:GPLv3
Web Site:
Source Code:https://github.com/jagossel/MovingPolygons
Issue Tracker:https://github.com/jagossel/MovingPolygons/issues

Auto Name:Moving Polygons
Summary:Bouncing lines live wallpaper
Description:
No description available
.

Repo Type:git
Repo:https://github.com/jagossel/MovingPolygons.git

Build:1.0.0.3,4
    commit=v1.0.0.3

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.0.3
Current Version Code:4

