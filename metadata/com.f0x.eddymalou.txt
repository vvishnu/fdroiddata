Categories:Multimedia
License:GPLv3
Web Site:https://github.com/F0x06/EddyMalou
Source Code:https://github.com/F0x06/EddyMalou
Issue Tracker:https://github.com/F0x06/EddyMalou/issues

Auto Name:Eddy Malou
Summary:Eddy Malou sound board
Description:
Eddy Malou sound board
.

Repo Type:git
Repo:https://github.com/F0x06/EddyMalou.git

Build:1.0,1
    commit=1.0

Build:2.0,2
    commit=2.0

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.0
Current Version Code:2

