Categories:Reading
License:AGPLv3+
Web Site:http://mupdf.com
Source Code:http://git.ghostscript.com/?p=mupdf.git;a=summary
Issue Tracker:http://bugs.ghostscript.com

Auto Name:MuPDF
Summary:Lightweight document viewer
Description:
'''N.B''' The names of the versions correspond to the chip architectures that
the apk is designed to run on.
This is the only PDF viewer we have that is designed for MIPS; the ARM apk
contains libraries for both ARM and ARMv7a.

MuPDF supports PDF 1.7 with transparency, encryption, hyperlinks, annotations,
searching, form editing and more.
It also reads OpenXPS and CBZ (comic book) documents.
.

Repo Type:git
Repo:http://git.ghostscript.com/mupdf.git

Build:1.2,2
    commit=1.2
    subdir=android
    submodules=yes
    prebuild=cd .. && \
        make generate && \
        cd android
    buildjni=yes

Build:1.2-ARM,50
    commit=0516026e
    subdir=platform/android
    submodules=yes
    forceversion=yes
    prebuild=sed -i 's/armeabi-v7a/armeabi armeabi-v7a/g' jni/Application.mk && \
        cd ../.. && \
        make generate
    buildjni=yes

Build:1.2-x86,51
    commit=0516026e
    subdir=platform/android
    submodules=yes
    forceversion=yes
    forcevercode=yes
    patch=x86.patch
    prebuild=cd ../.. && \
        make generate
    buildjni=yes

Build:1.2-MIPS,52
    commit=0516026e
    subdir=platform/android
    submodules=yes
    forceversion=yes
    forcevercode=yes
    patch=mips.patch
    prebuild=cd ../.. && \
        make generate
    buildjni=yes

Build:1.4,53
    disable=Doesn't build, just hangs
    commit=1.4
    subdir=platform/android
    submodules=yes
    forceversion=yes
    prebuild=sed -i 's/armeabi-v7a/armeabi armeabi-v7a/g' jni/Application.mk && \
        cd ../.. && \
        make generate
    buildjni=yes

Maintainer Notes:
Source code is not up to date as far as android versioning goes. Our version
codes probably differ from the official ones.
.

Auto Update Mode:None
Update Check Mode:None
Current Version:1.4
Current Version Code:53

