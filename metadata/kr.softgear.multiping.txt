AntiFeatures:UpstreamNonFree
Categories:Internet
License:Apache2
Web Site:http://softgearko.blogspot.com
Source Code:https://github.com/softgearko/MultiPing-for-Android
Issue Tracker:https://github.com/softgearko/MultiPing-for-Android/issues

Auto Name:MultiPing
Summary:Ping multiple websites at once
Description:
No description available
.

Repo Type:git
Repo:https://github.com/softgearko/MultiPing-for-Android.git

Build:0.11,11
    commit=de335d
    rm=res/values/attrs.xml,libs/admob-sdk-android.jar
    prebuild=sed -i '47,53d' res/layout/main.xml && \
        sed -i '5s/3/4/g;7s/true/false/g' AndroidManifest.xml

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.11
Current Version Code:11

