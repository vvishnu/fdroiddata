Categories:Office
License:GPLv3+
Web Site:https://bitbucket.org/josten/reminder
Source Code:https://bitbucket.org/josten/reminder/src
Issue Tracker:

Auto Name:Reminder
Summary:Local and private reminders
Description:
Store reminders locally (and privately) without the need to use the
cloud; no need for a Google account. Currently there are two types
of reminders: continuous reminders and time based reminders, all of
which appear with a notification/vibrates at the specified interval.
.

Repo Type:git
Repo:https://bitbucket.org/josten/reminder.git

Build:1.1.2,1
    commit=aec7c98
    extlibs=android/android-support-v4.jar

Build:1.1.9,2
    commit=70e5fd2fd9752e
    extlibs=android/android-support-v4.jar

Maintainer Notes:
Switch to using tags once there is a tag with the correct version code.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.9
Current Version Code:2

